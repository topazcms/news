<?php

return [
    'slug' => '%TITLE%',
    'layouts' => [
        'post' => "Article Simple",
    ],
    'views' => [
        'recent' => 'news.recent',
    ],
];