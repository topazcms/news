# News module for Topaz CMS

## Installation

In config/app.php add `Topaz\News\TopazNewsServiceProvider::class` in the providers :

```php
['providers' => [
    Topaz\News\TopazNewsServiceProvider::class,
    Topaz\Core\TopazCatchallServiceProvider::class,  // Must always end Topaz modules providers
]
```

then run in a terminal :

```bash
$ php artisan vendor:publish --provider="Topaz\News\TopazNewsServiceProvider"
$ php artisan migrate
$ php artisan topaz:setup
$ php artisan topaz:themes
```


