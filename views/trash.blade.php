@extends('topaz::app')

@section('title', 'Articles')
@section('page_title', 'Articles')

@section('breadcrumb')
    @include('topaz::breadcrumb_item', ['title' => 'Actualités', 'url' => route('admin.news.posts.index')])
@endsection

@section('topbar_right')

    <ul class="nav nav-list nav-list-topbar pull-left mrn">
        <li>
            <a href="{{ route('admin.news.posts.index') }}">Pages actives</a>
        </li>
        <li class="active">
            <a href="{{ route('admin.news.posts.index', ['trashed' => 1]) }}"><i class="fa fa-trash-o"></i> Corbeille</a>
        </li>
    </ul>
    <div class="pull-left mt10 mr15 ml15 text-primary" style="font-size:0.5em">
        |
    </div>
    <div class="btn-group pull-left">
        <a class="btn btn-sm btn-primary text-bold" href="{{ route('admin.news.posts.add') }}">
            <i class="fa fa-plus"></i> Nouvel Article
        </a>
    </div>

@append

@section('breadcrumb_tail', 'Articles')

@section('body')
    <div class="panel panel-system panel-border top">
        <div class="panel-body pn">

        <div class="table-responsive">
            <table class="table table-hovered">
                <thead>
                    <tr>
                        <th>Titre / Extrait</th>
                        <th>Auteur</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($posts as $post)
                        <tr>
                            <td>
                                <h4><a href="{{ route('admin.news.posts.edit', $post) }}">{{ $post->title }}</a></h4>
                                {{ $post->excerpt }}
                                <div>
                                    <a href="{{ route('admin.news.posts.restore', $post) }}" class="btn btn-xs btn-success dark">
                                        <i class="fa fa-reply"></i> Restaurer l'article
                                    </a>
                                    <a href="{{ route('admin.news.posts.delete', $post) }}" class="btn btn-xs btn-danger">
                                        <i class="fa fa-times"></i> Supprimer définitivement
                                    </a>
                                </div>
                            </td>
                            <td>
                                {{ $post->author->displayname }}
                            </td>
                            <td>
                                Créé le {{ $post->created_at->format('d/m/Y H:i:s') }}
                                <br>
                                Mise à jour le {{ $post->updated_at->format('d/m/Y H:i:s') }}
                                <br>
                                <span class="text-danger">
                                    Supprimée le {{ $post->deleted_at->format('d/m/Y H:i:s') }}
                                </span>
                            </td>

                        </tr>
                    @empty
                        <tr>
                            <td colspan="10" class=" text-center pb35">
                                <div class="lead text-muted mt15">
                                    Aucun article supprimé
                                </div>
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="text-center">
            {!! $posts->render() !!}
        </div>

        </div>
    </div>
@stop