@extends('{THEME_ID}.app')

@section('body')
    <div class="page-header">
        <h3>
            {{ $post->title }}
        </h3>
        <div>
            Catégorie : <strong>{{ $post->category_name }}</strong> &mdash; Auteur : <strong>{{ $post->author_name }}</strong> &mdash; Publié le <strong>{{ $post->published_at->format('d/m/Y') }} à {{ $post->published_at->format('H:i') }}</strong>
        </div>
    </div>

    @if ($post->cover_id)
        <div class="text-center">
            <img src="{{ $post->cover->direct_url }}" alt="Couverture">
        </div>
    @endif

    <div class="introduction">
        {!! $post->introduction !!}
    </div>
    {!! $post->post_body !!}
@stop