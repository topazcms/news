@extends('{THEME_ID}.app')

@section('body')
    <div class="page-header"><h3><span class="text-primary">Les derniers articles</span></h3></div>

    <div class="row">
        @forelse ($posts as $post)
            <div class="col-md-6 well">
                <div class="pull-left">
                    <h4 style="margin-top:0">
                        <a href="{{ $post->url }}">{{ $post->title }}</a>
                    </h4>
                    {{ $post->excerpt }}
                    <br>
                    <a href="{{ $post->url }}">Lire l'article</a>
                </div>
                @if ($post->cover)
                    <div class="pull-right">
                        <img src="{{ $post->cover->image_url(['fit' => '64x64']) }}" alt="Couverture">
                    </div>
                @endif
                <div class="clearfix"></div>
            </div>
        @empty
            <div class="col-md-12">
                Aucun article n'a encore été publié !
            </div>
        @endforelse
    </div>
@stop