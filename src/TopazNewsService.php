<?php namespace Topaz\News;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Topaz\Core\Models\Menu;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Topaz\Core\Models\Resource;
use Topaz\Core\Services\SitesManager;
use Topaz\Core\Services\TopazService;
use Topaz\News\Models\Post;

class TopazNewsService {

    /** @var TopazService  */
    protected $topaz;

    /** @var SitesManager  */
    protected $sites;

    public function __construct()
    {
        $this->topaz = app('topaz');
        $this->sites = app('topaz.sites');
    }

    public function getLatest(\Topaz\Core\Services\RouteContext $context)
    {
        $nb = config('topaz_news.posts_per_page', 10);
        $posts = Post::published()->with('cover')->orderBy('created_at', 'DESC')->paginate($nb);

        $view_name = config('topaz_news.views.recent', 'news.recent');
        $view_name = $this->sites->getCurrentThemeView($view_name);
        if (!view()->exists($view_name)) {
            abort(404, "The view '{$view_name}' doesn't exists !");
        }

        $this->topaz->setPageTitle("Articles récents");

        return view($view_name, compact('posts'));
    }

} 