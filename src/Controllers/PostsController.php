<?php namespace Topaz\News\Controllers;

use Carbon\Carbon;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Topaz\Core\Controllers\Controller;
use Topaz\Core\Controllers\TopazCrudController;
use Topaz\Core\CrudForm\Checkbox;
use Topaz\Core\CrudForm\CKEditor;
use Topaz\Core\CrudForm\CrudForm;
use Topaz\Core\CrudForm\Datepicker;
use Topaz\Core\CrudForm\Image;
use Topaz\Core\CrudForm\Select2;
use Topaz\Core\CrudForm\Text;
use Topaz\Core\CrudForm\Toggle;
use Topaz\Core\Models\AdminUser;
use Topaz\Core\Models\Page;
use Topaz\Core\Models\User;
use Topaz\News\Models\Category;
use Topaz\News\Models\Post;
use Topaz\News\Models\Restriction;

class PostsController extends TopazCrudController {

    protected $modelName = Post::class;
    protected $route_prefix = 'admin.news.posts';

    protected $object_name_singular = 'article';
    protected $object_name_plural = 'articles';
    protected $object_name_male = true;

    protected $new_button = "Nouvel article";

    protected $color = 'system';

    protected $index_table = [
        'Titre/Extrait' => 'title_column',
        'Catégorie' => 'category_name',
        'Auteur' => 'author_name',
        'Dates' => 'dates_column',
    ];
    protected $index_sorting = [
        'Titre/Extrait' => 'title',
        'Dates' => 'updated_at',
    ];

    protected $validation_rules = [
        'title' => 'required',
        'author_id' => 'required|exists:topaz_users,id',
        'published_at' => 'date_format:d/m/Y H:i:s',
    ];

    //protected $update_validation_rules = [
    //    'position' => 'numeric|unique:onepage_sections,position,',
    //];

    protected $sort_string = '-created_at';
    protected $objectsPerPage = 20;

    public static function routes()
    {
        parent::crud_routes('posts', 'posts.', ['middleware' => 'permission:news.posts.manage']);
    }

    protected static function custom_routes()
    {
        Route::get('publish_now/{post_id}', ['as' => 'publish_now', 'uses' => get_called_class().'@publish_now']);
    }

    public function globalQuery($query)
    {
        return $query->currentSite();
    }

    public function indexQuery($query)
    {
        return $query->with('author');
    }

    public function searchQuery($query, $term)
    {
        $like_term = "%$term%";
        return $query->where(function ($q) use ($like_term) {
            $q->orWhere('title', 'like', $like_term);
        });
    }

    public function beforeAddForm($object)
    {
        $object->author_id = $this->user->id;
    }

    public function beforeCreateForm($object)
    {
        if (!empty($this->request->get('published_at'))) {
            $object->published_at = Carbon::createFromFormat('d/m/Y H:i:s', $this->request->get('published_at'));
        } else {
            $object->published_at = new Carbon;
        }
    }

    public function beforeUpdateForm($object)
    {
        if (!empty($this->request->get('published_at'))) {
            $object->published_at = Carbon::createFromFormat('d/m/Y H:i:s', $this->request->get('published_at'));
        } else {
            $object->published_at = new Carbon;
        }
    }

    /**
     * @param $object
     * @return CrudForm
     */
    public function getForm($object, $add)
    {
        $form = new CrudForm;

        $form->appendMain(with(new Text('title', null))->setPlaceholder("Titre de l'article")->big());
        $form->appendMain(with(new CKEditor('introduction', "Introduction <small class='text-muted'>(facultatif)</small>"))->rows(5)->noPickers());
        $form->appendMain(with(new CKEditor('post_body', "Contenu de l'article")));

        $form->appendSidebar(with(new Checkbox('draft', "Brouillon", "En mode Brouillon, un article n'est pas visible", 'warning')));
        $form->appendSidebar(with(new Datepicker('published_at', "<i class=\"fa fa-calendar\"></i> Date de publication", true))->setPlaceholder("Laisser vide pour définir automatiquement"));
        $form->appendSidebar(with(new Select2('category_id', Category::forUser($this->user), "<i class=\"fa fa-folder-open\"></i> Catégorie")));
        $form->appendSidebar(with(new Select2('author_id', User::authors(), "<i class=\"fa fa-user\"></i> Auteur")));

        $form->appendSidebar(with(new Select2('layout', config('topaz_news.layouts', ['post'=>'Article Simple']), "<i class=\"fa fa-list-alt mr5\"></i> Mise en page"))->noSearch());
        $form->appendSidebar(with(new Image('cover_id', $object->cover, "<i class=\"fa fa-photo mr5\"></i> Photo d'illustration")));

        return $form;
    }

    protected function generateActionFor($object)
    {
        $actions = [];
        if (!$object->is_published) {
            $actions[] = [
                'name' => "<i class='fa fa-exclamation-circle'></i> Publier maintenant !",
                'route' => 'admin.news.posts.publish_now'
            ];
        }

        return $actions;
    }

    public function publish_now($post_id)
    {
        $post = Post::findOrFail($post_id);

        $post->draft = false;
        $post->published_at = Carbon::now()->subSecond();
        $post->save();

        return $this->redirectToIndex();
    }
}
