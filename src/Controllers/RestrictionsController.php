<?php namespace Topaz\News\Controllers;

use Carbon\Carbon;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Topaz\Core\Controllers\Controller;
use Topaz\Core\Controllers\TopazCrudController;
use Topaz\Core\CrudForm\CKEditor;
use Topaz\Core\CrudForm\CrudForm;
use Topaz\Core\CrudForm\Datepicker;
use Topaz\Core\CrudForm\Image;
use Topaz\Core\CrudForm\Select2;
use Topaz\Core\CrudForm\Text;
use Topaz\Core\Models\AdminUser;
use Topaz\Core\Models\Page;
use Topaz\Core\Models\User;
use Topaz\News\Models\Category;
use Topaz\News\Models\Post;
use Topaz\News\Models\Restriction;

class RestrictionsController extends TopazCrudController {

    protected $modelName = Restriction::class;
    protected $route_prefix = 'admin.news.restrictions';

    protected $object_name_singular = 'restriction';
    protected $object_name_plural = 'restrictions';
    protected $object_name_male = false;

    protected $color = 'system';

    protected $index_table = [
        'Utilisateur' => 'user_column',
        'Catégories' => 'categories_column',
    ];
    protected $index_sorting = [
        'Utilisateur' => 'user_id',
    ];

    protected $validation_rules = [
        'user_id' => 'required|exists:topaz_users,id',
    ];

    //protected $update_validation_rules = [
    //    'position' => 'numeric|unique:onepage_sections,position,',
    //];

    protected $sort_string = '+user_id';
    protected $objectsPerPage = 20;

    public static function routes()
    {
        parent::crud_routes('restrictions', 'restrictions.', ['middleware' => 'permission:news.restrictions.manage']);
    }

    public function globalQuery($query)
    {
        return $query->currentSite()->select('news_restrictions.*');
    }

    public function indexQuery($query)
    {
        return $query;
    }

    public function searchQuery($query, $term)
    {
        $like_term = "%$term%";
        return $query->join('topaz_users', 'topaz_restrictions.user_id', '=', 'topaz_users.id')
                ->where(function ($q) use ($like_term) {
                    $q->orWhere('topaz_users.username', 'like', $like_term);
                    $q->orWhere('topaz_users.firstname', 'like', $like_term);
                    $q->orWhere('topaz_users.lastname', 'like', $like_term);
                });
    }


    /**
     * @param $object
     * @return CrudForm
     */
    public function getForm($object, $add)
    {
        $form = new CrudForm;

        $form->appendMain(with(new Select2('user_id', User::all()->lists('display_name', 'id')->all(), "<i class='fa fa-user'></i> Utilisateur")));
        $form->appendMain(with(new Select2('categories', Category::currentSite()->lists('name', 'id')->all(), "<i class='fa fa-folder-o'></i> Catégories autorisées"))->multiple());

        return $form;
    }
}
