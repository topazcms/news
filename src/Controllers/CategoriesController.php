<?php namespace Topaz\News\Controllers;

use Carbon\Carbon;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Topaz\Core\Controllers\Controller;
use Topaz\Core\Controllers\TopazCrudController;
use Topaz\Core\CrudForm\CKEditor;
use Topaz\Core\CrudForm\CrudForm;
use Topaz\Core\CrudForm\Datepicker;
use Topaz\Core\CrudForm\Image;
use Topaz\Core\CrudForm\Select2;
use Topaz\Core\CrudForm\Text;
use Topaz\Core\Models\AdminUser;
use Topaz\Core\Models\Page;
use Topaz\Core\Models\User;
use Topaz\News\Models\Category;
use Topaz\News\Models\Post;

class CategoriesController extends TopazCrudController {

    protected $modelName = Category::class;
    protected $route_prefix = 'admin.news.categories';

    protected $object_name_singular = 'catégorie';
    protected $object_name_plural = 'catégories';
    protected $object_name_male = false;

    protected $color = 'system';

    protected $index_table = [
        'Nom' => 'name',
        'Identifieur' => 'slug',
    ];
    protected $index_sorting = [
        'Nom' => 'name',
        'Identifieur' => 'slug',
    ];

    protected $validation_rules = [
        'name' => 'required',
    ];

    //protected $update_validation_rules = [
    //    'position' => 'numeric|unique:onepage_sections,position,',
    //];

    protected $sort_string = '+name';
    protected $objectsPerPage = 20;

    public static function routes()
    {
        parent::crud_routes('categories', 'categories.', ['middleware' => 'permission:news.categories.manage']);
    }

    public function globalQuery($query)
    {
        return $query->currentSite();
    }

    public function indexQuery($query)
    {
        return $query;
    }

    public function searchQuery($query, $term)
    {
        $like_term = "%$term%";
        return $query->where(function ($q) use ($like_term) {
            $q->orWhere('name', 'like', $like_term);
            $q->orWhere('slug', 'like', $like_term);
        });
    }


    /**
     * @param $object
     * @return CrudForm
     */
    public function getForm($object, $add)
    {
        $form = new CrudForm;

        $form->appendMain(with(new Text('name', "Nom de la catégorie"))->big());
        $form->appendMain(with(new Text('slug', "Identifieur", "Laissez vide si vous ne savez pas quoi mettre")));

        $form->appendSidebar(with(new Image('cover_id', $object->cover, "<i class=\"fa fa-photo mr5\"></i> Illustration par défaut")));
        $pages = Page::currentSite()->userRestricted()->get()->pluck('title', 'id')->prepend("(Aucune page liée)", 0)->toArray();
        $form->appendSidebar(with(new Select2('linked_page_id', $pages , "<i class=\"fa fa-link mr5\"></i> Page liée")));


        return $form;
    }
}
