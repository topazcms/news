<?php

namespace Topaz\News;

use Carbon\Carbon;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Topaz\Core\Commands\TopazGenerateSuperadmin;
use Topaz\Core\Middleware\TopazLogged;
use Topaz\Core\Middleware\TopazMinify;
use Topaz\Core\Models\Menu;
use Topaz\Core\Models\Page;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Topaz\Core\Services\TopazService;
use Topaz\Forms\Models\Form;
use Topaz\News\Models\Category;
use Topaz\News\Models\Post;
use Topaz\News\Models\Restriction;

class TopazNewsServiceProvider extends ServiceProvider
{
    use SEOToolsTrait;

    /**
     * @var TopazService $topaz
     */
    protected $topaz;

    protected function registerRoutes()
    {
        if (!$this->app->routesAreCached()) {
            require __DIR__ . '/routes.php';
        }
    }

    protected function registerViews()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'topaz_news');
    }

    protected function registerMiddlewares()
    {
        $router = $this->app['router'];
//        $router->middleware('logged', TopazLogged::class);
    }

    protected function registerCommands()
    {
//        $this->registerCommand('superadmin', TopazGenerateSuperadmin::class);
    }

    protected function registerCommand($id, $cmd)
    {
        $this->app->singleton('command.topaz.'.$id, function ($app) use ($cmd) {
            return $app[$cmd];
        });
        $this->commands('command.topaz.'.$id);
    }

    protected function publishAssets()
    {
        $this->publishes([
            __DIR__ . '/../migrations/' => base_path('/database/migrations')
        ], 'migrations');

        $this->publishes([
            __DIR__ . '/../public/' => public_path('topaz'),
        ], 'assets');
    }

    protected function publishConfigs()
    {
        $this->publishes([
            __DIR__ . '/../config/topaz_news.php' => config_path('topaz_news.php')
        ], 'config');

        $this->mergeConfigFrom( __DIR__ . '/../config/topaz_news.php', 'topaz_news');
    }

    private function registerBladeDirectives()
    {
//        Blade::directive('navbaritem', function($expr) {
//            $expr = str_replace('(', '', $expr);
//            $expr = str_replace(')', '', $expr);
//            $expr = str_replace('\'', '', $expr);
//
//            $params = array_map(function($val) { return trim($val); }, explode(',', $expr));
//            dd($params);
//            $li_tag = (Route::currentRouteName() == $routename) ? 'li class="active"' : 'li';
//            return "<$li_tag><a href='".route($routename, $route_params)."'><span class='glyphicon glyphicon-$icon'></span><span class='sidebar-title'>$title</span></a></li>";
//        });
    }

    protected function registerService()
    {
        $this->app->singleton('topaz.news', function ($app) {
            return new TopazNewsService();
        });

         $this->topaz->registerRouteType("Actualités", 'newspaper-o', 'news_latest', "Derniers articles", 'star', 'topaz.news', 'getLatest');

        $this->topaz->registerAdminMenuItem([
            'icon' => 'newspaper-o',
            'title' => 'Actualités',
            'active' => 'news*',
            'route' => 'admin.news.posts.index',
            'order' => '10',
            'items' => [
                [
                    'icon' => 'newspaper-o',
                    'title' => 'Articles',
                    'active' => 'news/posts*',
                    'route' => 'admin.news.posts.index',
                    'permission' => 'news.posts.manage',
                ],
                [
                    'icon' => 'folder-open',
                    'title' => 'Catégories',
                    'active' => 'news/categories*',
                    'route' => 'admin.news.categories.index',
                    'permission' => 'news.categories.manage',
                ],
                [
                    'icon' => 'user-times',
                    'title' => 'Restrictions',
                    'active' => 'news/restrictions*',
                    'route' => 'admin.news.restrictions.index',
                    'permission' => 'news.restrictions.manage',
                ],
            ]
        ]);

        $this->topaz->registerResourceType(Post::class);

        $this->topaz->permissions()->needPermission('news.posts.manage', "Gérer les articles", ['admin', 'redactor']);
        $this->topaz->permissions()->needPermission('news.categories.manage', "Gérer les catégories des articles", ['admin']);
        $this->topaz->permissions()->needPermission('news.restrictions.manage', "Gérer les restrictions des articles", ['admin']);

        Post::saving(function($post) {
            if ($post->published_at === null || $post->published_at->year == -1) {
                $post->published_at = Carbon::now()->format('Y-m-d H:i:s');
            }
        });

        Post::creating(function($post) {
            $post->site_id = app('topaz.sites')->getCurrentSiteId();
        });

        Post::created(function($post) {
            $slug = config('topaz_news.slug');

            $slug = str_replace('%TITLE%', str_slug($post->title), $slug);

            $slug = str_slug($slug);
            $post->addRoute($slug);
        });

        Category::creating(function($category) {
            $category->site_id = app('topaz.sites')->getCurrentSiteId();
        });

        Category::saving(function($category) {
            if (empty($category->slug)) {
                $category->slug = str_slug($category->name);
            }
        });

        Restriction::creating(function($restriction) {
            $restriction->site_id = app('topaz.sites')->getCurrentSiteId();
        });

        $this->topaz->sites()->onDuplicate(function($new_site, $source_site) {
            foreach (Post::ofSite($source_site)->get() as $post) {
                $this->topaz->sites()->duplicateEntity($new_site, $post);
            }
            foreach (Category::ofSite($source_site)->get() as $category) {
                $this->topaz->sites()->duplicateEntity($new_site, $category);
            }
            foreach (Restriction::ofSite($source_site)->get() as $restriction) {
                $this->topaz->sites()->duplicateEntity($new_site, $restriction);
            }
        });

        $this->topaz->sites()->onDelete(function($site) {
            foreach (Post::ofSite($site)->get() as $post) {
                $post->forceDelete();
            }
            foreach (Category::ofSite($site)->get() as $category) {
                $category->delete();
            }
            foreach (Restriction::ofSite($site)->get() as $restriction) {
                $restriction->delete();
            }
        });

        $this->topaz->sites()->onThemeCreation(function($cp) {
            $layouts = config('topaz_news.layouts');

            foreach ($layouts as $id => $name) {
                $cp(__DIR__ . '/../resources/views/post.blade.php', 'news/' . $id . '.blade.php');
            }
            $cp(__DIR__ . '/../resources/views/recent.blade.php', 'news/recent.blade.php');
        });

    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->topaz = app('topaz');
        $this->registerRoutes();
        $this->registerViews();
        $this->registerMiddlewares();
        $this->registerCommands();
        $this->registerBladeDirectives();
        $this->registerService();

        $this->publishAssets();
        $this->publishConfigs();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
