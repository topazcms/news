<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'tpz-admin/news', 'as' => 'admin.news.', 'middleware' => 'logged:AdminUser'], function() {

    \Topaz\News\Controllers\PostsController::routes();
    \Topaz\News\Controllers\CategoriesController::routes();
    \Topaz\News\Controllers\RestrictionsController::routes();

});