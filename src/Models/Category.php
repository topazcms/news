<?php namespace Topaz\News\Models;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Topaz\Core\Models\HasSite;
use Topaz\Core\Models\Media;
use Topaz\Core\Models\Page;
use Topaz\Core\Models\ResourceableInterface;
use Topaz\Core\Models\ResourceableModel;
use Topaz\Core\Models\User;
use Topaz\Core\Services\RouteContext;
use Topaz\Core\Services\TopazService;

class Category extends Model {

    use HasSite;

	protected $table = 'news_categories';
//    protected $route_prefix = 'page/';
	protected $fillable = ['name', 'slug', 'cover_id', 'linked_page_id'];

    public $timestamps = false;

    public function __toString()
    {
        return $this->name;
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function cover()
    {
        return $this->belongsTo(Media::class);
    }

    public function linked_page()
    {
        return $this->belongsTo(Page::class);
    }

    public static function forUser(User $user)
    {

        $restriction = Restriction::whereUserId($user->id)->first();
        if ($restriction !== null) {
            $query = self::currentSite()->whereIn('id', $restriction->categories);
        } else {
            $categories[0] = "(Aucune catégorie)";
            $query = self::currentSite();
        }

        foreach ($query->get() as $category) {
            $categories[$category->id] = $category->name;
        }

        return $categories;
    }

}
