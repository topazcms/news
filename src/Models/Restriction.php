<?php namespace Topaz\News\Models;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Topaz\Core\Models\HasSite;
use Topaz\Core\Models\Media;
use Topaz\Core\Models\ResourceableInterface;
use Topaz\Core\Models\ResourceableModel;
use Topaz\Core\Models\User;
use Topaz\Core\Services\RouteContext;
use Topaz\Core\Services\TopazService;

class Restriction extends Model {

    use HasSite;

	protected $table = 'news_restrictions';
	protected $fillable = ['user_id', 'categories'];

    public $timestamps = false;

    public function __toString()
    {
        return $this->user_column;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getCategoriesAttribute()
    {
        return explode(',', $this->categories_ids);
    }

    public function setCategoriesAttribute($value)
    {
        $this->attributes['categories_ids'] = implode(',', $value);
    }

    public function getRealCategoriesAttribute()
    {
        return Category::whereIn('id', $this->categories)->get();
    }

    public function getUserColumnAttribute()
    {
        if ($this->user === null) {
            return "";
        }

        return "{$this->user->display_name} ({$this->user->username})";
    }

    public function getCategoriesColumnAttribute()
    {
        return $this->real_categories->implode('name', ', ');
    }





}
