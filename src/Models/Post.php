<?php namespace Topaz\News\Models;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Topaz\Core\Models\HasSite;
use Topaz\Core\Models\Media;
use Topaz\Core\Models\ResourceableInterface;
use Topaz\Core\Models\ResourceableModel;
use Topaz\Core\Models\User;
use Topaz\Core\Services\RouteContext;
use Topaz\Core\Services\TopazBreadcrumb;
use Topaz\Core\Services\TopazService;

class Post extends ResourceableModel implements ResourceableInterface {

    use SoftDeletes, SEOToolsTrait, HasSite;

	protected $table = 'news_posts';
//    protected $route_prefix = 'page/';
	protected $fillable = [
        'title',
        'post_body',
        'author_id',
        'cover_id',
        'layout',
        'introduction',
        'category_id',
        'draft',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'published_at',
    ];

    protected $casts = [
        'draft' =>  'bool'
    ];

    public function __toString()
    {
        return $this->title;
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function cover()
    {
        return $this->belongsTo(Media::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getResourceIcon()
    {
        return 'newspaper-o';
    }

    public function getResourceTitle()
    {
        return 'Article';
    }

    public function getResourceResponse(RouteContext $context)
    {
        global $app;

        /** @var TopazService $topaz */
        $topaz = $app['topaz'];
        $topaz->setPageTitle($this->title);

        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('article:author', $this->authorName);
        $this->seo()->opengraph()->addProperty('article:published_time', $this->created_at->format(\DateTime::ISO8601));
        $this->seo()->opengraph()->addProperty('article:modified_time', $this->updated_at->format(\DateTime::ISO8601));
        if ($this->trashed())
            $this->seo()->opengraph()->addProperty('article:expiration_time', $this->deleted_at->format(\DateTime::ISO8601));

        /** @var TopazBreadcrumb $breadcrumb */
        $breadcrumb = app('topaz.breadcrumb');

        if ($this->category !== null) {
            $linked_page = $this->category->linked_page;
            if ($linked_page !== null) {
                $breadcrumb->append($linked_page);
            }
        }

        $breadcrumb->append($this);

        if (\Auth::admin()->guest() && !$this->is_published) {
            return app('topaz.routing')->notFound();
        }

        return view(app('topaz.sites')->getCurrentThemeView('news.' . $this->layout), ['post' => $this]);
    }

    public function getResources()
    {
        return static::orderBy('updated_at', 'desc')->get();
    }

    public function getExcerptAttribute()
    {
        if (!empty($this->introduction)) {
            return strip_tags($this->introduction);
        }

        return Str::words(strip_tags($this->post_body));
    }

    public function setPostBodyAttribute($value)
    {
        $minifier = app('htmlmin');
        $value = $minifier->html($value);
        $value = app('topaz')->parseHtml($value);
        $this->attributes['post_body'] = $value;
    }

    public function getPostBodyAttribute()
    {
        if (!array_key_exists('post_body', $this->attributes)) {
            return null;
        }
        $post_body = $this->attributes['post_body'];
        $post_body = app('topaz')->unparseHtml($post_body);
        return $post_body;
    }

    public function getTitleColumnAttribute()
    {
        $edit_route = route('admin.news.posts.edit', $this);
        $res = "<h4><a href='$edit_route'>{$this->title}</a></h4>";

        if ($this->draft) {
            $res .= "<div><span class='label label-default'><i class='fa fa-file-o'></i> Brouillon</span>{$this->excerpt}</div>";
        } else {
            $res .= "<div>{$this->excerpt}</div>";
        }

        return $res;
    }

    public function getDatesColumnAttribute()
    {
        $dates = [
            "Créé le {$this->created_at->format('d/m/Y à H:i:s')}",
            "Modifié le {$this->updated_at->format('d/m/Y à H:i:s')}",
        ];


        if ($this->trashed()) {
            $dates[] = "<span class='text-danger'>Supprimé le {$this->deleted_at->format('d/m/Y à H:i:s')}</span>";
        } elseif ($this->is_published) {
            $dates[] = "<span class='text-primary text-bold'>Publié le {$this->published_at->format('d/m/Y à H:i:s')}</span>";
        } else {
            $dates[] = "<span class='text-warning text-bold'>Sera publié le {$this->published_at->format('d/m/Y à H:i:s')}</span>";
        }

        return implode(array_map(function($val) { return "<div>$val</div>"; }, $dates));
    }

    public function getAuthorNameAttribute()
    {
        if ($this->author === null) {
            return null;
        }

        return $this->author->displayname;
    }

    public function getCategoryNameAttribute()
    {
        if ($this->category === null) {
            return "(Aucune Catégorie)";
        }

        return $this->category->name;
    }

    public function getIsPublishedAttribute()
    {
        return $this->published_at->isPast() && !$this->draft;
    }

    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', date('Y-m-d H:i:s'))->whereDraft(false);
    }

    public function scopeWithRestrictions($query)
    {
        $user = app('auth')->admin()->get();
        // Check if user has restrictions
        $restriction = Restriction::whereUserId($user->id)->first();
        if ($restriction !== null) {
            $query = $query->where(function($q) use ($restriction, $user) {
                return $q->whereIn('category_id', $restriction->categories)->orWhere('author_id', $user->id);
            });
        }

        return $query;
    }

    public function getCoverOrDefaultAttribute()
    {
        $cover = $this->cover;
        if ($cover !== null) {
            return $cover;
        }

        $category = $this->category;

        if ($category !== null) {
            return $category->cover;
        }

        return null;
    }
}
